//
//  ViewController.swift
//  randomIq
//
//  Created by manish on 9/15/21.
//

import UIKit

class ViewController: UIViewController {

    let stringtoCompress = "AAABBACCFFAABB"
    var compressedString = ""
    var letterCountStored:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        task1()
        task2()
        task3()
    }
    
    //Write a program to find the k largest elements in a given array
    func task1() {
        let randomNumber = [1,95,21,12,23,45,20,02,91,911]
         let sortedNumber =  randomNumber.sorted {
            return $0 > $1
        }
        print(sortedNumber)
        
    }
    
    func task2() {
        
        //let  n number
        let randomN = Int.random(in:  1..<50)
        var numbers: [Int] = []
        for _ in 0..<randomN {
            let number = Int.random(in: 1..<10)
            numbers.append(number)
        }
        
        let uniqueNumbers = Set(numbers)
        
        print(numbers)
        print("unique numbers:\(Set(uniqueNumbers))")
    }
    
    func task3() {
        let compressedStringOutput = compressString(string2Compress: stringtoCompress)
        print(compressedStringOutput)
    }
    


    // function the compress given string
    func compressString(string2Compress: String) ->(String) {
        letterCountStored = 0
        
        // get string length
        let letterCount = string2Compress.count
      
        // check if string has more than 1 letter
        if letterCount == 1 {
            compressedString = string2Compress
        } else {
            let stringlenth = letterCount-1
            let stringArray = Array(string2Compress)
            
            for i in 0 ..< stringlenth {
                let firstLetterToCompare = stringArray[i]
                let secondLetterToCompare = stringArray[i + 1]
                
                //pass the above 2 letters to function to compare
                compareTwoConsecutiveLetters(firstLetter: firstLetterToCompare, secondLetter: secondLetterToCompare)
                
                //Check if comparing the last two letters
                if i == stringlenth - 1 {
                    // append last letter count to compressed string
                    appendLetterCount()
                }
            }
        }
        
        compressedString = (compressedString.count > string2Compress.count) ? compressedString : string2Compress
        return compressedString
    }

    // function to compare two consecutive letters
    func compareTwoConsecutiveLetters(firstLetter : Character, secondLetter : Character){
        
        // chcek if letters are same
        if firstLetter == secondLetter {
            
            // add one to the letter count ie 0 to 1
            letterCountStored += 1
            
            //check if string is empty
            if compressedString.isEmpty == true {
                
                //add first letter to compressed string
                compressedString = compressedString + String(firstLetter)
                
                // since first 2 letters are same add one to the letter count ie 1 to 2
                letterCountStored += 1
            }
        }
        else {
            
            // check if compressed string is empty
            if compressedString.isEmpty == true {
                
                // add one to the letter count
                letterCountStored += 1
                
                // append first letter to compressed string
                compressedString = compressedString + String(firstLetter)
                
                // append first letter count to compressed string
                appendLetterCount()
                
                // append second letter to compressed string
                compressedString = compressedString + String(secondLetter)
                
                // reset the letter count
                letterCountStored = 1
                
                
            }
                // if compressed string alredy has first letter
            else if compressedString.count == 1 {
                
                /// append first letter count to compressed string
                appendLetterCount()
                
                // append second letter to compressed string
                compressedString = compressedString + String(secondLetter)
                
                // reset the letter count
                letterCountStored = 1
            }
            else {
                
                // append first letter count to compressed string
                appendLetterCount()
                
                // append second letter to compressed string
                compressedString = compressedString + String(secondLetter)
                
                // reset the letter count
                letterCountStored = 1
            }
        }
    }



    // function to append letter count
    func appendLetterCount(){
        
        // check if letter occurs more than once
        if letterCountStored > 1 {
            //append to compressed string
            compressedString = compressedString + String(letterCountStored)
            
        }
    }
}

